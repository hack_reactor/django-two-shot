from django.urls import path
from receipts.views import (
    all_receipts,
    create_receipt,
    list_categories,
    list_accounts,
    create_category,
    create_account,
)

urlpatterns = [
    path("", all_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", list_categories, name="category_list"),
    path("accounts/", list_accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
